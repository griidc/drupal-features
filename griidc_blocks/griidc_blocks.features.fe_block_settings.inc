<?php
/**
 * @file
 * griidc_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function griidc_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-leading_by_example'] = array(
    'cache' => -1,
    'css_class' => 'leading-by-example',
    'custom' => 0,
    'machine_name' => 'leading_by_example',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 1,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => 'GRIIDC Data Stories, News, and Updates',
    'visibility' => 0,
  );

  $export['block-copyright_info'] = array(
    'cache' => -1,
    'css_class' => 'copyright_info',
    'custom' => 0,
    'machine_name' => 'copyright_info',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-footer_links'] = array(
    'cache' => -1,
    'css_class' => 'footer_links',
    'custom' => 0,
    'machine_name' => 'footer_links',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-help'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'help',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
