<?php
/**
 * @file
 * griidc_blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function griidc_blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'GRIIDC Data Stories, News, and Updates';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'leading_by_example';
  $fe_block_boxes->body = 'Showcasing Data Management Activities in the Gulf of Mexico';

  $export['leading_by_example'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Copyright Information';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'copyright_info';
  $fe_block_boxes->body = '<p>Harte Research Institute for Gulf of Mexico Studies | Texas A&M University-Corpus Christi<br>Copyright © 2015 The Gulf of Mexico Research Initiative</p>';

  $export['copyright_info'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer Links';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_links';
  $fe_block_boxes->body = '<div class="logo"><img src="/sites/all/themes/griidc/images/griidc-logo-white.png" alt="GRIIDC"></div>
<div class="address">6300 Ocean Drive<br>Corpus Christi, TX 78412</div>
<div class="telephone"><a href="tel:3618253604">(361) 825-3604</a></div>
<div class="mail"><a href="mailto:griidc@gomri.org">griidc@gomri.org</a></div>
<div class="social"><a href="http://www.twitter.com/griidc">@griidc</a></div>';

  $export['footer_links'] = $fe_block_boxes;

  return $export;
}
