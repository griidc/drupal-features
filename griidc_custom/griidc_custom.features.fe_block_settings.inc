<?php
/**
 * @file
 * griidc_custom.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function griidc_custom_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-article_archive-block'] = array(
    'cache' => -1,
    'css_class' => 'sidebar-menu',
    'custom' => 0,
    'delta' => 'article_archive-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['views-board_roster-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'board_roster-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-frontpage_carousel-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'frontpage_carousel-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
