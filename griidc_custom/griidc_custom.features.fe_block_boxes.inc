<?php
/**
 * @file
 * griidc_custom.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function griidc_custom_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Copyright Information';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'copyright_info';
  $fe_block_boxes->body = '<p>Harte Research Institute for Gulf of Mexico Studies | Texas A&M University-Corpus Christi<br>Copyright © 2014 The Gulf of Mexico Research Initiative</p>';

  $export['copyright_info'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer Links (temporary)';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_links';
  $fe_block_boxes->body = '<img src="/sites/all/themes/griidc/images/footer-links.jpg">';

  $export['footer_links'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Leading by example Block';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'leading_by_example';
  $fe_block_boxes->body = 'We’ve gathered a few stories to showcase how sharing data can benefit the scientific community.';

  $export['leading_by_example'] = $fe_block_boxes;

  return $export;
}
