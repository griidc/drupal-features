<?php
/**
 * @file
 * griidc_custom.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function griidc_custom_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-data-discovery.
  $menus['menu-data-discovery'] = array(
    'menu_name' => 'menu-data-discovery',
    'title' => 'Data Discovery',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Data Discovery');
  t('Main menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');


  return $menus;
}
