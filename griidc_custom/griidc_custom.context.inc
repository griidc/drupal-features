<?php
/**
 * @file
 * griidc_custom.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function griidc_custom_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'articles';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'article' => 'article',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'archive/*' => 'archive/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-article_archive-block' => array(
          'module' => 'views',
          'delta' => 'article_archive-block',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['articles'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'board_roster';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'members' => 'members',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'board_roster' => 'board_roster',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['board_roster'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'frontpage';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-data-discovery' => array(
          'module' => 'menu',
          'delta' => 'menu-data-discovery',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'griidc_custom-pelagos_datasets' => array(
          'module' => 'griidc_custom',
          'delta' => 'pelagos_datasets',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'content_bottom',
          'weight' => '0',
        ),
        'views-frontpage_carousel-block' => array(
          'module' => 'views',
          'delta' => 'frontpage_carousel-block',
          'region' => 'content_bottom',
          'weight' => '1',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['frontpage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'griidc_pelagos';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'pelagos' => 'pelagos',
        'pelagos/*' => 'pelagos/*',
        'data-discovery' => 'data-discovery',
        'data-discovery/*' => 'data-discovery/*',
        'dataset-monitoring' => 'dataset-monitoring',
        'dataset-monitoring/*' => 'dataset-monitoring/*',
        'stats' => 'stats',
        'stats/*' => 'stats/*',
        'data' => 'data',
        'data/*' => 'data/*',
        'dataset-registration' => 'dataset-registration',
        'dataset-registration/*' => 'dataset-registration/*',
        'dataset-submission' => 'dataset-submission',
        'dataset-submission/*' => 'dataset-submission/*',
        'dif' => 'dif',
        'dif/*' => 'dif/*',
        'registry' => 'registry',
        'registry/*' => 'registry/*',
        'mdapp' => 'mdapp',
        'mdapp/*' => 'mdapp/*',
        'doi' => 'doi',
        'doi/*' => 'doi/*',
        'doi-request' => 'doi-request',
        'doi-request/*' => 'doi-request/*',
        'metadata-editor' => 'metadata-editor',
        'metadata-editor/*' => 'metadata-editor/*',
        'xml-validator' => 'xml-validator',
        'xml-validator/*' => 'xml-validator/*',
      ),
    ),
  );
  $context->reactions = array(
    'region' => array(
      'griidc' => array(
        'disable' => array(
          'footer' => 'footer',
          'header' => 0,
          'navigation' => 0,
          'highlighted' => 0,
          'help' => 0,
          'content_top' => 0,
          'content' => 0,
          'content_bottom' => 0,
          'sidebar_first' => 0,
          'sidebar_second' => 0,
        ),
      ),
    ),
    'template_suggestions' => 'page__pelagos__full',
    'theme_html' => array(
      'class' => 'page-pelagos-full',
    ),
  );
  $context->condition_mode = 0;
  $export['griidc_pelagos'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'griidc_pelagos_wide';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array();
  $context->reactions = array(
    'template_suggestions' => 'page__pelagos__wide',
    'theme_html' => array(
      'class' => 'page-pelagos-wide',
    ),
  );
  $context->condition_mode = 0;
  $export['griidc_pelagos_wide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'pelagos-pelagos_login_logout_links' => array(
          'module' => 'pelagos',
          'delta' => 'symfony_login_logout_links',
          'region' => 'header',
          'weight' => '-11',
        ),
        'pelagos-pelagos_login_logout_links' => array(
          'module' => 'pelagos',
          'delta' => 'pelagos_login_logout_links',
          'region' => 'header',
          'weight' => '-10',
        ),
        'superfish-1' => array(
          'module' => 'superfish',
          'delta' => '1',
          'region' => 'navigation',
          'weight' => '-9',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-2' => array(
          'module' => 'block',
          'delta' => '2',
          'region' => 'footer',
          'weight' => '1',
        ),
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'footer',
          'weight' => '0',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['sitewide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sub_pages';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        '~griidc_pelagos' => '~griidc_pelagos',
      ),
    ),
    'node' => array(
      'values' => array(
        'page' => 'page',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['sub_pages'] = $context;

  return $export;
}
