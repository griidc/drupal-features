<?php
/**
 * @file
 * griidc_custom.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function griidc_custom_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about-us:node/6
  $menu_links['main-menu_about-us:node/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'About Us',
    'options' => array(
      'identifier' => 'main-menu_about-us:node/6',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_monitor-data:node/7
  $menu_links['main-menu_monitor-data:node/7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/7',
    'router_path' => 'node/%',
    'link_title' => 'Monitor Data',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'nav-chart',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_monitor-data:node/7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_research:node/10
  $menu_links['main-menu_research:node/10'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/10',
    'router_path' => 'node/%',
    'link_title' => 'Research',
    'options' => array(
      'identifier' => 'main-menu_research:node/10',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_search-data:node/9
  $menu_links['main-menu_search-data:node/9'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/9',
    'router_path' => 'node/%',
    'link_title' => 'Search Data',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'nav-chart',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_search-data:node/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_submit-data-sub:node/2
  $menu_links['main-menu_submit-data-sub:node/2'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'Submit Data Sub',
    'options' => array(
      'identifier' => 'main-menu_submit-data-sub:node/2',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_submit-data:node/8',
  );
  // Exported menu link: main-menu_submit-data:node/8
  $menu_links['main-menu_submit-data:node/8'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/8',
    'router_path' => 'node/%',
    'link_title' => 'Submit Data',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'nav-chart',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_submit-data:node/8',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_tools--resources:node/5
  $menu_links['main-menu_tools--resources:node/5'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Tools & Resources',
    'options' => array(
      'identifier' => 'main-menu_tools--resources:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_tools-sub:node/3
  $menu_links['main-menu_tools-sub:node/3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Tools Sub',
    'options' => array(
      'identifier' => 'main-menu_tools-sub:node/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_tools--resources:node/5',
  );
  // Exported menu link: menu-data-discovery_monitor-data:<front>
  $menu_links['menu-data-discovery_monitor-data:<front>'] = array(
    'menu_name' => 'menu-data-discovery',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Monitor Data',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'monitor',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-data-discovery_monitor-data:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-data-discovery_search-data:<front>
  $menu_links['menu-data-discovery_search-data:<front>'] = array(
    'menu_name' => 'menu-data-discovery',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Search Data',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'search',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-data-discovery_search-data:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-data-discovery_submit-data:<front>
  $menu_links['menu-data-discovery_submit-data:<front>'] = array(
    'menu_name' => 'menu-data-discovery',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Submit Data',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'submit',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-data-discovery_submit-data:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About Us');
  t('Home');
  t('Monitor Data');
  t('Research');
  t('Search Data');
  t('Submit Data');
  t('Submit Data Sub');
  t('Tools & Resources');
  t('Tools Sub');


  return $menu_links;
}
