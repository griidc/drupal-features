<?php
/**
 * @file
 * griidc_menus.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function griidc_menus_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-data-discovery'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'menu-data-discovery',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => 'Ensuring a Data Legacy',
    'visibility' => 0,
  );

  $export['menu_block-1'] = array(
    'cache' => -1,
    'css_class' => 'sidebar-menu',
    'custom' => 0,
    'delta' => 1,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['menu_block-2'] = array(
    'cache' => -1,
    'css_class' => 'sidebar-menu',
    'custom' => 0,
    'delta' => 2,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['pelagos-pelagos_login_logout_links'] = array(
    'cache' => 1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'pelagos_login_logout_links',
    'module' => 'pelagos',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['superfish-1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 1,
    'module' => 'superfish',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'griidc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'griidc',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
