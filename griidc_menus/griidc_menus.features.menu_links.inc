<?php
/**
 * @file
 * griidc_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function griidc_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_-issue-tracking-in-jira:https://triton.tamucc.edu/issues
  $menu_links['main-menu_-issue-tracking-in-jira:https://triton.tamucc.edu/issues'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'https://triton.tamucc.edu/issues',
    'router_path' => '',
    'link_title' => ' Issue Tracking in JIRA',
    'options' => array(
      'identifier' => 'main-menu_-issue-tracking-in-jira:https://triton.tamucc.edu/issues',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: main-menu_about-gomri:node/1
  $menu_links['main-menu_about-gomri:node/1'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'About GoMRI',
    'options' => array(
      'identifier' => 'main-menu_about-gomri:node/1',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_about-us:<nolink>',
  );
  // Exported menu link: main-menu_about-griidc:node/6
  $menu_links['main-menu_about-griidc:node/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'About GRIIDC',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_about-griidc:node/6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_about-us:<nolink>',
  );
  // Exported menu link: main-menu_about-us:<nolink>
  $menu_links['main-menu_about-us:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'About Us',
    'options' => array(
      'identifier' => 'main-menu_about-us:<nolink>',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_account-request:account/new
  $menu_links['main-menu_account-request:account/new'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'account/new',
    'router_path' => 'account',
    'link_title' => 'Account Request',
    'options' => array(
      'identifier' => 'main-menu_account-request:account/new',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: main-menu_data-compliance-information:node/83
  $menu_links['main-menu_data-compliance-information:node/83'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/83',
    'router_path' => 'node/%',
    'link_title' => 'Data Compliance Information',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_data-compliance-information:node/83',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -42,
    'customized' => 0,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: main-menu_data-file-transfer-methods:node/39
  $menu_links['main-menu_data-file-transfer-methods:node/39'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/39',
    'router_path' => 'node/%',
    'link_title' => 'Data File Transfer Methods',
    'options' => array(
      'identifier' => 'main-menu_data-file-transfer-methods:node/39',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: main-menu_data-management-planning:node/37
  $menu_links['main-menu_data-management-planning:node/37'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/37',
    'router_path' => 'node/%',
    'link_title' => 'Data Management Planning',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_data-management-planning:node/37',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: main-menu_dataset-information-form:dif
  $menu_links['main-menu_dataset-information-form:dif'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dif',
    'router_path' => 'dif',
    'link_title' => 'Dataset Information Form',
    'options' => array(
      'identifier' => 'main-menu_dataset-information-form:dif',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_submit-data:<nolink>',
  );
  // Exported menu link: main-menu_dataset-monitoring:dataset-monitoring
  $menu_links['main-menu_dataset-monitoring:dataset-monitoring'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dataset-monitoring',
    'router_path' => 'dataset-monitoring',
    'link_title' => 'Dataset monitoring',
    'options' => array(
      'identifier' => 'main-menu_dataset-monitoring:dataset-monitoring',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_tracking--stats:<nolink>',
  );
  // Exported menu link: main-menu_dataset-submission-form:dataset-submission
  $menu_links['main-menu_dataset-submission-form:dataset-submission'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dataset-submission',
    'router_path' => 'dataset-submission',
    'link_title' => 'Dataset Submission Form',
    'options' => array(
      'identifier' => 'main-menu_dataset-submission-form:dataset-submission',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_submit-data:<nolink>',
  );
  // Exported menu link: main-menu_doi-request-form:doi-request
  $menu_links['main-menu_doi-request-form:doi-request'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'doi-request',
    'router_path' => 'doi-request',
    'link_title' => 'DOI Request Form',
    'options' => array(
      'identifier' => 'main-menu_doi-request-form:doi-request',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_submit-data:<nolink>',
  );
  // Exported menu link: main-menu_faqs:faq-page
  $menu_links['main-menu_faqs:faq-page'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'faq-page',
    'router_path' => 'faq-page',
    'link_title' => 'FAQs',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_faqs:faq-page',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: main-menu_griidc-server-use-analytics:https://data.gulfresearchinitiative.org/piwik
  $menu_links['main-menu_griidc-server-use-analytics:https://data.gulfresearchinitiative.org/piwik'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'https://data.gulfresearchinitiative.org/piwik',
    'router_path' => '',
    'link_title' => 'GRIIDC server use analytics',
    'options' => array(
      'identifier' => 'main-menu_griidc-server-use-analytics:https://data.gulfresearchinitiative.org/piwik',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_tracking--stats:<nolink>',
  );
  // Exported menu link: main-menu_griidc-system-statistics:stats
  $menu_links['main-menu_griidc-system-statistics:stats'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'stats',
    'router_path' => 'stats',
    'link_title' => 'GRIIDC system statistics',
    'options' => array(
      'identifier' => 'main-menu_griidc-system-statistics:stats',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_tracking--stats:<nolink>',
  );
  // Exported menu link: main-menu_gulf-of-mexico-data-sources:node/33
  $menu_links['main-menu_gulf-of-mexico-data-sources:node/33'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/33',
    'router_path' => 'node/%',
    'link_title' => 'Gulf of Mexico Data Sources',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_gulf-of-mexico-data-sources:node/33',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 0,
    'parent_identifier' => 'main-menu_search-data:<nolink>',
  );
  // Exported menu link: main-menu_help:<nolink>
  $menu_links['main-menu_help:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Help',
    'options' => array(
      'identifier' => 'main-menu_help:<nolink>',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_how-to-submit-data:node/2
  $menu_links['main-menu_how-to-submit-data:node/2'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'How to Submit Data',
    'options' => array(
      'identifier' => 'main-menu_how-to-submit-data:node/2',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_submit-data:<nolink>',
  );
  // Exported menu link: main-menu_iso-19115-2-metadata-editor:metadata-editor-start
  $menu_links['main-menu_iso-19115-2-metadata-editor:metadata-editor-start'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'metadata-editor-start',
    'router_path' => 'metadata-editor-start',
    'link_title' => 'ISO 19115-2 Metadata Editor',
    'options' => array(
      'identifier' => 'main-menu_iso-19115-2-metadata-editor:metadata-editor-start',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_submit-data:<nolink>',
  );
  // Exported menu link: main-menu_password-reset:account/password
  $menu_links['main-menu_password-reset:account/password'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'account/password',
    'router_path' => 'account',
    'link_title' => 'Password Reset',
    'options' => array(
      'identifier' => 'main-menu_password-reset:account/password',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: main-menu_research:http://research.gulfresearchinitiative.org/
  $menu_links['main-menu_research:http://research.gulfresearchinitiative.org/'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://research.gulfresearchinitiative.org/',
    'router_path' => '',
    'link_title' => 'Research',
    'options' => array(
      'attributes' => array(
        'title' => 'GoMRI Research Information',
        'name' => 'GoMRI Research',
        'target' => '_blank',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_research:http://research.gulfresearchinitiative.org/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: main-menu_search-data:<nolink>
  $menu_links['main-menu_search-data:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Search Data',
    'options' => array(
      'identifier' => 'main-menu_search-data:<nolink>',
      'attributes' => array(
        'class' => array(
          0 => 'nav-chart',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_search-griidc-data:data-discovery
  $menu_links['main-menu_search-griidc-data:data-discovery'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'data-discovery',
    'router_path' => 'data-discovery',
    'link_title' => 'Search GRIIDC Data',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_search-griidc-data:data-discovery',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'main-menu_search-data:<nolink>',
  );
  // Exported menu link: main-menu_submit-data:<nolink>
  $menu_links['main-menu_submit-data:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Submit Data',
    'options' => array(
      'identifier' => 'main-menu_submit-data:<nolink>',
      'attributes' => array(
        'class' => array(
          0 => 'nav-chart',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_tracking--stats:<nolink>
  $menu_links['main-menu_tracking--stats:<nolink>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Tracking & Stats',
    'options' => array(
      'identifier' => 'main-menu_tracking--stats:<nolink>',
      'attributes' => array(
        'class' => array(
          0 => 'nav-chart',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_training--user-guides:node/38
  $menu_links['main-menu_training--user-guides:node/38'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/38',
    'router_path' => 'node/%',
    'link_title' => 'Training & User Guides',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_training--user-guides:node/38',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'main-menu_help:<nolink>',
  );
  // Exported menu link: menu-data-discovery_monitor-data:dataset-monitoring
  $menu_links['menu-data-discovery_monitor-data:dataset-monitoring'] = array(
    'menu_name' => 'menu-data-discovery',
    'link_path' => 'dataset-monitoring',
    'router_path' => 'dataset-monitoring',
    'link_title' => 'Monitor Data',
    'options' => array(
      'identifier' => 'menu-data-discovery_monitor-data:dataset-monitoring',
      'attributes' => array(
        'class' => array(
          0 => 'monitor',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-data-discovery_search-data:data-discovery
  $menu_links['menu-data-discovery_search-data:data-discovery'] = array(
    'menu_name' => 'menu-data-discovery',
    'link_path' => 'data-discovery',
    'router_path' => 'data-discovery',
    'link_title' => 'Search Data',
    'options' => array(
      'identifier' => 'menu-data-discovery_search-data:data-discovery',
      'attributes' => array(
        'class' => array(
          0 => 'search',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-data-discovery_submit-data:node/2
  $menu_links['menu-data-discovery_submit-data:node/2'] = array(
    'menu_name' => 'menu-data-discovery',
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'Submit Data',
    'options' => array(
      'identifier' => 'menu-data-discovery_submit-data:node/2',
      'attributes' => array(
        'class' => array(
          0 => 'submit',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t(' Issue Tracking in JIRA');
  t('About GRIIDC');
  t('About GoMRI');
  t('About Us');
  t('Account Request');
  t('DOI Request Form');
  t('Data Compliance Information');
  t('Data File Transfer Methods');
  t('Data Management Planning');
  t('Dataset Information Form');
  t('Dataset Submission Form');
  t('Dataset monitoring');
  t('FAQs');
  t('GRIIDC server use analytics');
  t('GRIIDC system statistics');
  t('Gulf of Mexico Data Sources');
  t('Help');
  t('Home');
  t('How to Submit Data');
  t('ISO 19115-2 Metadata Editor');
  t('Monitor Data');
  t('Password Reset');
  t('Research');
  t('Search Data');
  t('Search GRIIDC Data');
  t('Submit Data');
  t('Tracking & Stats');
  t('Training & User Guides');


  return $menu_links;
}
